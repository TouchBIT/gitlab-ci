SHELL=/bin/bash -o pipefail
BEFORE_DISK_FREE=$$(df -h /)

docker-clean:
	@echo Останавливаем все контейнеры
	docker kill $$(docker ps -q) || true
	@echo Очистка докер контейнеров
	docker rm -f $$(docker ps -a -f status=exited -q) || true
	@echo Очистка dangling образов
	docker rmi -f $$(docker images -f "dangling=true" -q) || true
	@echo Очистка образов
	docker rmi -f $$(docker images-q) || true
	@echo Очистка volume
	docker volume rm -f $$(docker volume ls -q) || true
	@echo Очистка сетей
	docker network prune -f || true
	@echo "Занятость диска до очистки:"
	@echo "${BEFORE_DISK_FREE}"
	@echo "Занятость диска после очистки:"
	@echo "$$(df -h /)"